#include <stdio.h>

int main ()
{
	int a = 0;
	#pragma omp parallel
	{
		#pragma omp critical
		{
			a++;
		}
	}
	printf("%d\n", a);
	return 0;
}