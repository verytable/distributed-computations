#include <stdio.h>

int main ()
{
	int a = 123;
	#pragma omp parallel firstprivate(a)
	{
		a++;
		printf("local %d\n", a);
	}
	printf("%d\n", a);
	return 0;
}