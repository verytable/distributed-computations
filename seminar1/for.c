#include <stdio.h>
#include <omp.h>

int main ()
{
	#pragma omp parallel
	{
		int i = 0;
		#pragma omp for schedule(dynamic) //static(по умолчанию) распределение не будет менятся по ходу, dynamic - в runtime
		for (i = 0; i < 10; i++) 
		{
			printf("%d -> %d\n", omp_get_thread_num(), i);
		}
	}
	return 0;
}