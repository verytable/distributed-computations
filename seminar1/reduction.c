#include <stdio.h>
#include <omp.h>

int main ()
{
	int sum = 0;
	#pragma omp parallel
	{
		int i = 0;
		#pragma omp for reduction(+: sum)
		for (i = 0; i < 10; i++) {
			sum += i;
		}
	}
	printf("%d\n", sum);
	return 0;
}