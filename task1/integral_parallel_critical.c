#include <stdio.h>
#include <omp.h>
#include <math.h>

double f(double x) {
    return sqrt(4 - x * x);
}

double get_area(double left, double right) {
    return (f(right) + f(left)) * (right - left) / 2.0;
}

int main(int argc, char* argv[]) {
    double steps = atoi(argv[1]);
    double sum = 0.0;
    #pragma omp parallel
    {
        double local_sum = 0.0;
        double j = omp_get_thread_num();
        double m = omp_get_num_threads();
        double start = (j * 4.0) / m;
        int i;
        for (i = 0; i < steps / m; ++i) {
            double left = -2.0 + start + (4 * i) / steps;
            double right = -2.0 + start + (4 * (i + 1)) / steps;
            if (left < -2.0) {
                left = -2.0;
            }
            if (right > 2.0) {
                right = 2.0;
            }
            local_sum += get_area(left, right);
        }
        #pragma omp critical
        {
            sum += local_sum;
        }    
    }
    printf("%.6f\n", sum);
}

                  
                  
              
