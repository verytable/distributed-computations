#include <stdio.h>
#include <omp.h>
#include <math.h>

double area(double x1, double x2) {
    return (sqrt(4 - x1 * x1) + sqrt(4 - x2 * x2)) * (x2 - x1) / 2.0;
}

double integrate(double N) {
    double sum = 0.0;
    #pragma omp parallel
    {
        double threads = omp_get_num_threads();
        double num = omp_get_thread_num();
        double s = 0.0;
        double x0 = (2.0 * num) / threads;
        int i;
        for (i = 0; i < N / threads; ++i) {
            double x1 = x0 + (2.0 * i) / N;
            double x2 = x0 + (2.0 * (i + 1)) / N;
            if (x1 > 2.0) {
                x1 = 2.0;
            }
            if (x2 > 2.0) {
                x2 = 2.0;
            }
            s += area(x1, x2);
        }
        #pragma omp critical
        {
            sum += s;
        }    
    }
    return sum;
}

int main(int argc, char* argv[]) {
    double N = atoi(argv[1]);
    printf("%.8f\n", integrate(N));
}
