#include <stdio.h>
#include <omp.h>
#include <math.h>

double f(double x) {
    return sqrt(4 - x * x);
}

double get_area(double left, double right) {
    return (f(right) + f(left)) * (right - left) / 2.0;
}

int main(int argc, char* argv[])
{
    int steps = atoi(argv[1]);
    int i;
    double sum = 0.0;
    #pragma omp parallel for reduction(+: sum)
    for (i = 0; i < steps; ++i) {
        sum += get_area(-2.0 + i * 4.0 / steps, -2.0 + (i + 1) * 4.0 / steps);
    }
    printf("%.6f\n", sum);
}

