#include <stdio.h>
#include <omp.h>
#include <math.h>

double area(double x1, double x2) {
    return (sqrt(4 - x1 * x1) + sqrt(4 - x2 * x2)) * (x2 - x1) / 2.0;
}

double integrate(int N)
{
    double sum = 0.0;
    int i;
    #pragma omp parallel for reduction(+: sum)
    for (i = 0; i < N; ++i) {
        sum += area(2.0 * i / N, 2.0 * (i + 1) / N);
    }
    return sum;
}

int main(int argc, char** argv) {
    int N = atoi(argv[1]);
    printf("%.8f\n", integrate(N));
}

