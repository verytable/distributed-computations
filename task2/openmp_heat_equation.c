#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>

#define L 1.0
#define k 0.3

int main(int argc, char const *argv[]) {
    struct timespec t0;
    struct timespec t1;
    clock_gettime(CLOCK_REALTIME, &t0);
    int N = atoi(argv[1]);
    double T = atof(argv[2]);
    double h = L / N;
    double tau = k * h * h;
    int S = T / tau;
    double *u, *u1, *t;
    int i, j;

    u = (double*)malloc(sizeof(double) * N);
    u1 = (double*)malloc(sizeof(double) * N);

    for (i = 0; i < N; ++i) {
        u[i] = u1[i] = 0.0;
    }

    u[N - 1] = 1.0;
    u1[N - 1] = 1.0;

    #pragma omp parallel private(i, j)
    for (j = 0; j < S; ++j) {
        #pragma omp for schedule(dynamic)
        for (i = 1; i < N - 1; ++i) {
            u1[i] = u[i] - k * (2.0 * u[i] - u[i - 1] - u[i + 1]);
        }
        #pragma omp single
        {
            t = u;
            u = u1;
            u1 = t;
        }
    }

    if (N < 100) {
        for (i = 0; i < N; ++i) {
            printf("%.4f %.8f\n", h * i, u[i]);
        }
    }
    free(u);
    free(u1);
    clock_gettime(CLOCK_REALTIME, &t1);
    printf("It took me %d seconds.\n", (int) (t1.tv_sec - t0.tv_sec));
    return 0;
}

