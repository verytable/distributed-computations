#include <mpi.h>
#include <stdio.h>
#include <omp.h>
#include <math.h>

double f(double x) {
    return sqrt(4 - x * x);
}

double get_area(double left, double right) {
    return (f(right) + f(left)) * (right - left) / 2.0;
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    double steps = atoi(argv[1]);
    int i;
    double local_sum = 0.0;
    for (i = 0; i < steps; ++i) {
        if (i % size == rank) {
            double left = -2.0 + (4 * i) / steps;
            double right = -2.0 + (4 * (i + 1)) / steps;
            if (left < -2.0) {
                left = -2.0;
            }
            if (right > 2.0) {
                right = 2.0;
            }
            local_sum += get_area(left, right);
        }
    }
    if (rank != 0) {
        MPI_Send(&local_sum, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
    if (rank == 0) {
	double sum = 0.0;
        for (i = 1; i < size; ++i) {
            MPI_Recv(&sum, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            local_sum += sum;
        }
        printf("%f\n", local_sum);
    }
    MPI_Finalize();
    return 0;
}

