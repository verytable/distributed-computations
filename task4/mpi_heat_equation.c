#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

#define L 1.0
#define k 0.3

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    int N = atoi(argv[1]) - 1;
    double T = atof(argv[2]);
    double h = L / (N + 1);
    double tau = k * h * h;
    int S = (int) (T / tau);
    double *u, *u1, *t;
    int i, j;

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (N < size) {
            size = N;
    }

    if (rank >= size) {
            MPI_Finalize();
            return 0;
    }
    int points_num = N / size;
    if (rank < N % size) {
            points_num += 1;
    }

    u = (double*)malloc(sizeof(double) * (points_num + 2));
    u1 = (double*)malloc(sizeof(double) * (points_num + 2));

    for (i = 0; i < points_num + 2; ++i) {
            u[i] = u1[i] = 0.0;
    }

    if (rank == size - 1) {
            u[points_num + 1] = 1.0;
    }

    if (rank == size - 1) {
            u1[points_num + 1] = 1.0;
    }

    for (j = 0; j < S; ++j) {
        int start = 1;
        int finish = points_num;
        for (i = start; i <= finish; ++i) {
                u1[i] = u[i] - k * (2.0 * u[i] - u[i - 1] - u[i + 1]);
        }
        t = u;
        u = u1;
        u1 = t;

        if (rank % 2 == 0) {
            if (rank != size - 1) {
                MPI_Ssend(&u[points_num], 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD);
                MPI_Recv(&u[points_num + 1], 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
            if (rank != 0) {
                MPI_Ssend(&u[1], 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);
                MPI_Recv(&u[0], 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        } else {
            MPI_Recv(&u[0], 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Ssend(&u[1], 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);
            if (rank != size - 1) {
                MPI_Recv(&u[points_num + 1], 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Ssend(&u[points_num], 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD);
            }
        }
    }

    double *ans;
    if (rank == 0) {
        ans = (double*)malloc(sizeof(double) * N);
        int i;
        for (i = 1; i <= points_num; ++i) {
            ans[i - 1] = u[i];
        }
        for (i = 1; i < N % size; ++i) {
            MPI_Recv((ans + points_num + (i - 1) * points_num), points_num, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        if (N % size == 0) {
            for (i = 1; i < size; ++i) {
                    MPI_Recv(ans + i * points_num, points_num, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        } else {
            for (i = N % size; i < size; ++i) {
                MPI_Recv(ans + (N % size) * points_num + (i - N % size) * (points_num - 1),
                points_num - 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        }
        int max = N;
        if (max > 10) {
            max = 10;
        }
        for (i = 0; i < max; ++i) {
            printf("%.2f %.8f\n", h * (i + 1), ans[i]);
        }
        free(ans);
    } else {
        MPI_Send(&u[1], points_num, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
    free(u);
    free(u1);
    MPI_Finalize();
    return 0;
}

